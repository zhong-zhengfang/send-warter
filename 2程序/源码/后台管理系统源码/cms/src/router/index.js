import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home',
    component: () => import('@/views/index'),
    children: [
      {
        path: 'home',
        component: () => import('@/views/home')
      },
      {
        path: 'user-info',
        component: () => import('@/views/user/userInfo.vue')
      },
      {
        path: 'user-avatar',
        component: () => import('@/views/user/userAvatar.vue')
      },
      {
        path: 'user-pwd',
        component: () => import('@/views/user/userPwd.vue')
      },
      {
        path: 'goods-manage',
        component: () => import('@/views/goods/goodsManage.vue')
      },
      {
        path: 'orders-list',
        component: () => import('@/views/orders/orders-list.vue')
      },
      {
        path: 'vips-manage',
        component: () => import('@/views/vips/vips-manage.vue')
      },
      {
        path: 'deposit-manage',
        component: () => import('@/views/deposit/deposit-manage.vue')
      },
      {
        path: 'sides-manage',
        component: () => import('@/views/sides/sides-manage.vue')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login')
  },
  {
    path: '/reg',
    component: () => import('@/views/register')
  }
]

const whiteList = ['/login', '/reg']

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (whiteList.includes(to.path)) {
    next()
    return
  }
  if (store.state.token.token) {
    next()
  } else {
    next('/login')
  }
})
export default router
