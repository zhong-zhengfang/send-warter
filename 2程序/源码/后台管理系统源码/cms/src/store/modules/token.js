export default {
  namespaced: true,
  state: {
    token: ''
  },
  getters: {
    getToken (state) {
      return state.token
    }
  },
  mutations: {
    setToken (state, val) {
      state.token = val
    }
  },
  actions: {

  }
}
