export default {
  namespaced: true,
  state: {
    userInfo: {}
  },
  getters: {
    getUserInfo (state) {
      return state.userInfo
    }
  },
  mutations: {
    setUserInfo (state, val) {
      state.userInfo = val
    }
  },
  actions: {

  }
}
