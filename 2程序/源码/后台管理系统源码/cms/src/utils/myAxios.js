import axios from 'axios'
import store from '../store/index'
import router from '@/router'
const myAxios = axios.create({
  baseURL: 'http://localhost:3000'
})

myAxios.interceptors.request.use((config) => {
  if (store.state.token.token) {
    config.headers.Authorization = store.state.token.token
  }
  return config
}, (err) => {
  return Promise.reject(err)
})

myAxios.interceptors.response.use((response) => {
  if (response.data.message === '身份认证失败') {
    store.commit('token/setToken', '')
    router.push('/login')
    alert('登录已过期，请重新登录')
  }
  return response
}, (err) => {
  store.commit('token/setToken', '')
  console.log(err)
  alert(err)
  return Promise.reject(err)
})
export default myAxios
