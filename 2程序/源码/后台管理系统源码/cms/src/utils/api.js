import myAxios from '@/utils/myAxios.js'
import qs from 'qs'

// 注册
export const register = (option) => {
  return myAxios({
    url: '/api/reguser',
    method: 'POST',
    data: qs.stringify(option)
  })
}
// 登录
export const login = (option) => {
  return myAxios({
    url: '/api/login',
    method: 'POST',
    data: qs.stringify(option)
  })
}

// 获取用户信息
export const getUerInfo = () => {
  return myAxios({
    url: '/my/userinfo'

  })
}

// 修改用户信息
export const modifyUerInfo = (option) => {
  return myAxios({
    url: '/my/userinfo',
    method: 'POST',
    data: qs.stringify(option)

  })
}

// 上传/修改头像
export const modifyAvatar = (option) => {
  return myAxios({
    url: '/my/updateavatar',
    method: 'POST',
    data: qs.stringify(option)

  })
}

// 修改密码
export const modifyPwd = (option) => {
  return myAxios({
    url: '/my/updatepwd',
    method: 'POST',
    data: qs.stringify(option)

  })
}

// 获取分类
export const getCates = () => {
  return myAxios({
    url: '/article/cates'

  })
}

// 新增分类
export const addCates = (option) => {
  return myAxios({
    url: '/article/addcates',
    method: 'POST',
    data: qs.stringify(option)

  })
}

// 修改分类
export const updateCates = (option) => {
  return myAxios({
    url: '/article/updatecate',
    method: 'PUT',
    data: qs.stringify(option)

  })
}

// 删除分类
export const deleteCates = (option) => {
  return myAxios({
    url: '/article/deletecate',
    method: 'PUT',
    data: qs.stringify(option)

  })
}
