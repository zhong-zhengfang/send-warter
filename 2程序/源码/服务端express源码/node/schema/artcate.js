//导入验证规则模块
const joi = require('joi')

//定义规则
const name = joi.string().required()
const alias = joi.string().alphanum().required()
const aid = joi.number().integer().min(1).required()

exports.add_cates_schema = {
    body:{
        name,
        alias
    }
}

exports.delete_cates_schema = {
    body:{
        aid
    }
}

exports.get_cates_schema = {
    params:{
        aid
    }
}

exports.update_cates_schema = {
    body:{
        aid,
        name,
        alias
    }
}