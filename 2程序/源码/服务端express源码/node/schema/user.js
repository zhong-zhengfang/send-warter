const joi = require('joi')

//定义自定义规则
const username = joi.string().min(1).max(10).required()
const password = joi.string().pattern(/^[\S]{6,12}$/).required()
const repassword = joi.string().pattern(/^[\S]{6,12}$/).required()
const id = joi.number().integer().min(1).required()
const c_nickname = joi.string().required()
const email = joi.string().email().required()
const avatar = joi.string().dataUri().required()
const c_phone = joi.required()

exports.login_schema = { //登录的验证规则
    body: {
        username,
        password
    }
}

exports.reg_schema = { //注册的验证规则
    body: {
        username,
        password,
        repassword
    }
}

exports.update_userinfo_schema = { //更新用户信息验证规则
    body: {
        c_nickname,
        email,
        c_phone
    }
}

exports.update_password_schema = { //更新密码验证规则
    body: {
        oldPwd: password,
        newPwd: joi.not(joi.ref('oldPwd')).concat(password),
        reNewPwd: password
    }
}

exports.update_avatar_schema = { //更新头像验证规则
    body: {
        avatar
    }
}