const joi = require('joi')

//定义自定义规则
const c_account = joi.string().min(1).max(10).required()
const c_password = joi.string().pattern(/^[\S]{6,12}$/).required()
const s_account = joi.string().min(1).max(10).required()
const s_password = joi.string().pattern(/^[\S]{6,12}$/).required()
const v_account = joi.string().min(11).max(11).required()
const v_password = joi.string().pattern(/^[\S]{6,12}$/).required()
const id = joi.number().integer().min(1).required()
const nickname = joi.string().required()
const email = joi.string().email().required()
exports.cms_login_schema = { //登录的验证规则
    body: {
        c_account,
        c_password
    }
}

exports.side_login_schema = { //登录的验证规则
    body: {
        s_account,
        s_password
    }
}

exports.vip_login_schema = { //登录的验证规则
    body: {
        v_account,
        v_password
    }
}

exports.update_userinfo_schema = { //更新用户信息验证规则
    body: {
        nickname,
        email
    }
}

exports.update_password_schema = { //更新密码验证规则
    body: {
        oldPwd: c_password,
        newPwd: joi.not(joi.ref('oldPwd')).concat(c_password),
        reNewPwd: c_password
    }
}