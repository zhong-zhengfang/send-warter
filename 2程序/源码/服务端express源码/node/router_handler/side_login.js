//导入数据库连接池模块
const db = require('../db/index')


//导入jsonwebtoken
const jwt = require('jsonwebtoken')
const config = require('../config')

exports.sidelogin = (req, res) => {
    console.log(req.body)
    const userInfo = req.body
    const sql = 'select * from side_login_info where s_account=?'
    db.query(sql,userInfo.s_account,(err, result) => {
        if(err) return res.cc(err)
        if(result.length !== 1) return res.cc('登录失败！')

        const loginFlag = userInfo.s_password === result[0].s_password
        if(!loginFlag) {
           return res.cc('账号或密码错误！')
        }
        const user = {...result[0],s_password:''}
        console.log(user)
        const tokenStr = jwt.sign(user,config.jwtSecretKey,{expiresIn:'10h'})
        res.send({
            message:'登录成功',
            status:0,
            token:'Bearer '+tokenStr
        })
    })
}