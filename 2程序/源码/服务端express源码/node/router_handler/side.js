//导入数据库连接池
const db = require('../db/index')

//获取水站列表
exports.getSideList = (req, res) => {
    const sql = 'select * from side_cms'
    db.query(sql,(err, result) => {
        if(err) return res.cc(err)
        res.send({
            status:0,
            massage:'查询站点成功',
            data:result

        })
    })

}

//获取对应的水站信息
exports.getSideInfo = (req, res) => {
    const sql = 'select * from side_cms where sid =?'
    db.query(sql,req.user.sid,(err, result) => {
        if(err) return res.cc(err)
        res.send({
            status:0,
            massage:'查询站点成功',
            data:result

        })
    })

}

//修改水站信息
exports.modifySideInfo = (req, res) => {
    console.log(req.body)
    const sql = "update side_cms set? where sid=?"
    db.query(sql, [req.body,req.user.sid], (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('修改水站信息失败')
        res.cc('修改水站信息成功', 0)
    })
}

//更新用户密码
exports.updatePassword = (req, res) => {
    console.log(req.body)
    const sql = 'select * from side_login_info where sid=?'
    db.query(sql,req.user.sid,(err, result) => {
        if(err) return res.cc(err)
        if(result.length !== 1) return res.cc('用户不存在')
        
       const compareResult = req.body.oldPwd === result[0].s_password
       if(!compareResult) return res.cc('旧密码错误')
       const sql = 'update side_login_info set s_password=? where sid=?'
       if(req.body.newPwd !== req.body.reNewPwd) return res.cc('两次密码不一致')
       db.query(sql,[req.body.newPwd, req.user.sid],(err, result) => {
        if(err) return res.cc(err)
        if(result.affectedRows !== 1) return res.cc('更新密码失败')

        res.cc('更新密码成功',0)

       })
    })

}
