//导入数据库连接池
const db = require('../db/index')

//获取对应的用户信息
exports.getVipInfo = (req, res) => {
    const sql = 'select * from vip_cms where vid =?'
    db.query(sql, req.user.vid, (err, result) => {
        if (err) return res.cc(err)
        res.send({
            status: 0,
            massage: '查询用户信息成功',
            data: result

        })
    })

}

//获取对应的用户信息
exports.getmyOrder = (req, res) => {
    const sql = 'select * from order_cms where vid =? and ostatus=?'
    db.query(sql, [req.user.vid, req.body.ostatus], (err, result) => {
        if (err) return res.cc(err)
        res.send({
            status: 0,
            massage: '查询用户订单信息成功',
            data: result

        })
    })

}

function rad(d) {
    return d * Math.PI / 180.0;
}
function distanceOf(p1, p2) {
    var radLng1 = rad(p1.x);
    var radLng2 = rad(p2.x);
    var mdifference = radLng1 - radLng2;
    var difference = rad(p1.y) - rad(p2.y);
    var distance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(difference / 2), 2)
        + Math.cos(radLng1) * Math.cos(radLng2) * Math.pow(Math.sin(mdifference / 2), 2)));
    distance = distance * 6378.137;
    distance = Math.abs(Math.round(distance * 10000) / 10);
    return distance;
}
var dis = distanceOf({ "x": 104.06765, "y": 30.54169 }, { "x": 104.06705, "y": 30.55769 });


//获取当前时间
function getNowTime() {
    var date = new Date();
    //年 getFullYear()：四位数字返回年份
    var year = date.getFullYear();  //getFullYear()代替getYear()
    //月 getMonth()：0 ~ 11
    var month = date.getMonth() + 1;
    //日 getDate()：(1 ~ 31)
    var day = date.getDate();
    //时 getHours()：(0 ~ 23)
    var hour = date.getHours();
    //分 getMinutes()： (0 ~ 59)
    var minute = date.getMinutes();
    //秒 getSeconds()：(0 ~ 59)
    var second = date.getSeconds();

    var time = year + '-' + addZero(month) + '-' + addZero(day) + ' ' + addZero(hour) + ':' + addZero(minute) + ':' + addZero(second);
    return time;
}


//小于10的拼接上0字符串
function addZero(s) {
    return s < 10 ? ('0' + s) : s;
}

//发起订单请求
exports.addOrder = (req, res) => {
    const sql = 'select sid,slot,slat from side_cms'
    db.query(sql, (err, result) => {
        if (err) return res.cc(err)
        const vobj = {}
        vobj.x = req.body.olot
        vobj.y = req.body.olat
        let arr = result
        for (let i = 0; i < arr.length; i++) {
            let sobj = {}
            sobj.x = arr[i].slot
            sobj.y = arr[i].slat
            let res = distanceOf(vobj, sobj)
            arr[i].dist = res
        }
        arr.sort((a, b) => a.dist - b.dist)
        let sssid = arr[0].sid
        const sql2 = 'select sname,sphone from side_cms where sid =?'
        db.query(sql2, arr[0].sid, (err, result) => {
            if (err) return res.cc(err)
            let pre = {}
            console.log(result)
            pre.osendside = result[0].sname
            pre.osendphone = result[0].sphone
            pre.sid = sssid
            pre.vid = req.user.vid
            pre.otime = getNowTime()
            let inserObj = { ...pre, ...req.body }
            console.log(inserObj)
            const sql3 = 'insert into order_cms set ?'
            db.query(sql3, inserObj, (err, result) => {
                if (err) return res.cc(err)
                if (result.affectedRows !== 1) return res.cc('提交订单失败')
                res.cc('提交订单成功', 0)
            })
        })
    })

}


