//导入数据库操作模块
const db = require('../db/index')

//导入密码处理模块
const bcrypt = require('bcryptjs')

//获取用户信息
exports.getUserInf = (req, res) => {

    //sql查询语句
    const sql = 'select cid,c_phone, c_nickname, email, from cms_user where cid=?' 

    db.query(sql, req.user.cid, (err, result) => {
        if(err) return res.cc(err)
        if(result.length !== 1) {
           return res.cc('查询失败')
        }
         res.send({
            status:0,
            message:'获取用户信息成功',
            data:result[0]
        })
    })
}

//更新用户信息
exports.updateUserInfo = (req, res) => {
    //sql查询语句
    const sql = 'update ev_user set ? where id=?' 

    db.query(sql, [req.body,req.user.id], (err, result) => {
        if(err) return res.cc(err)
        if(result.affectedRows !== 1) {
           return res.cc('更新用户信息失败')
        }
         res.cc('更新用户信息成功',0)
    })
}


//更新用户密码
exports.updatePassword = (req, res) => {
    console.log(req.body)
    const sql = 'select * from ev_user where id=?'
    db.query(sql,req.user.id,(err, result) => {
        if(err) return res.cc(err)
        if(result.length !== 1) return res.cc('用户不存在')
        
       const compareResult = bcrypt.compareSync(req.body.oldPwd,result[0].password)
       if(!compareResult) return res.cc('旧密码错误')
       const sql = 'update ev_user set password=? where id=?'
       if(req.body.newPwd !== req.body.reNewPwd) return res.cc('两次密码不一致')
       const newPwd = bcrypt.hashSync(req.body.newPwd)
       db.query(sql,[newPwd, req.user.id],(err, result) => {
        if(err) return res.cc(err)
        if(result.affectedRows !== 1) return res.cc('更新密码失败')

        res.cc('更新密码成功',0)

       })
    })

}


//更新用户头像
exports.updateAvatar = (req, res) => {
    const sql = 'update ev_user set user_pic=? where id=?'
    db.query(sql,[req.body.avatar, req.user.id], (err, result) => {
        if(err) return res.cc(err)
        if(result.affectedRows !== 1) {
            return res.cc('更新头像失败')
        }
        res.cc('更新头像成功',0)
    })
}
