//导入数据库连接池
const db = require('../db/index')

//获取订单列表
exports.getDepositList = (req, res) => {
    const sql = 'select * from cms_deposit where dstatus =0'
    db.query(sql,(err, result) => {
        if(err) return res.cc(err)
        res.send({
            status:0,
            massage:'查询退款申请记录成功成功',
            data:result

        })
    })

}

//退押金
exports.backDeposit = (req, res) => {
    console.log(req)
    const sql = "update cms_deposit set dstatus=1 where dvid=?"
    db.query(sql, req.body.dvid, (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('更新退押金失败')
        const sql = "update vip_cms set visdep='已退还' where vid=?"
        db.query(sql, req.body.dvid, (err, result) => {
            if (err) return res.cc(err)
            if (result.affectedRows !== 1) return res.cc('更新会员押金信息失败')
            res.cc('更新会员押金信息成功', 0)
        })
    })
}
