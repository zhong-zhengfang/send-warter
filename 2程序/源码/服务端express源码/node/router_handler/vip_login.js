//导入数据库连接池模块
const db = require('../db/index')


//导入jsonwebtoken
const jwt = require('jsonwebtoken')
const config = require('../config')

exports.viplogin = (req, res) => {
    console.log(req.body)
    const userInfo = req.body
    const sql = 'select * from vip_login where v_account=?'
    db.query(sql,userInfo.v_account,(err, result) => {
        if(err) return res.cc(err)
        if(result.length !== 1) return res.cc('登录失败！')

        const loginFlag = userInfo.v_password === result[0].v_password
        if(!loginFlag) {
           return res.cc('账号或密码错误！')
        }
        const user = {...result[0],v_password:''}
        console.log(user)
        const tokenStr = jwt.sign(user,config.jwtSecretKey,{expiresIn:'10h'})
        res.send({
            message:'登录成功',
            status:0,
            token:'Bearer '+tokenStr
        })
    })
}

//获取商品列表接口处理函数
exports.vipgetGoods = (req, res) => {
    const sql = 'select * from goods_cms where g_isdelete = 0 and g_status = "已上架"'
    db.query(sql,(err, result) => {
        if (err) return res.cc(err)
        res.send({
            status: 0,
            massage: '查询商品列表成功',
            data: result

        })
    })

}

//获取商品列表接口处理函数
exports.vipgetGoodsDetail = (req, res) => {
    const sql = 'select * from goods_cms where gid =?'
    db.query(sql,req.query.gid,(err, result) => {
        if (err) return res.cc(err)
        res.send({
            status: 0,
            massage: '查询商品详情成功',
            data: result

        })
    })

}