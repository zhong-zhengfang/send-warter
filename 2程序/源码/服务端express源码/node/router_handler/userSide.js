//导入数据库连接池
const db = require('../db/index')

//获取商品列表接口处理函数
exports.getGoodsList = (req, res) => {
    const sql = 'select * from goods_cms where g_isdelete = 0 and g_status = "已上架"'
    db.query(sql,(err, result) => {
        if (err) return res.cc(err)
        res.send({
            status: 0,
            massage: '查询商品列表成功',
            data: result

        })
    })

}