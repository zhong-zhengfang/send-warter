//导入数据库连接池
const db = require('../db/index')

//获取订单列表
exports.getappSideList = (req, res) => {
    const sql = 'select * from order_cms where sid =? and ostatus =?'
    db.query(sql,[req.user.sid,req.body.ostatus],(err, result) => {
        if(err) return res.cc(err)
        res.send({
            status:0,
            massage:'查询订单成功',
            data:result

        })
    })

}

//接单
exports.setOrderStart = (req, res) => {
    console.log(req.body.oid)
    const sql = "update order_cms set ostatus='待配送' where oid=?"
    db.query(sql, req.body.oid, (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('接单失败')
        res.cc('接单成功', 0)
    })
}

//已送达
exports.setOrderArrive = (req, res) => {
    console.log(req.body.oid)
    const sql = "update order_cms set ostatus='已送达' where oid=?"
    db.query(sql, req.body.oid, (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('接单失败')
        res.cc('接单成功', 0)
    })
}
