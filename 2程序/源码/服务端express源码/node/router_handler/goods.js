//导入数据库连接池
const db = require('../db/index')

//获取商品列表接口处理函数
exports.getGoods = (req, res) => {
    const sql = 'select * from goods_cms where g_isdelete = 0'
    db.query(sql, req.user.id, (err, result) => {
        if (err) return res.cc(err)
        res.send({
            status: 0,
            massage: '查询文章分类成功',
            data: result

        })
    })

}
//上传图片
exports.uploadPic = (req, res) => {
    res.send({
        code: 0,
        msg: '上传成功',
        data: `http://192.168.43.66:3000/${req.file.filename}`
    })

}
//新增文章分类接口处理函数
exports.addGoods = (req, res) => {
    const sql = 'insert into goods_cms set ?'
    db.query(sql, req.body, (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('新增商品失败')
        res.cc('新增商品成功', 0)
    })

}
//删除商品
exports.deleteGoods = (req, res) => {
    console.log(req)
    const sql = "update goods_cms set g_isdelete=1 where gid=?"
    db.query(sql, req.body.gid, (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('删除失败')
        res.cc('删除成功', 0)
    })
}

//上架商品
exports.setgoodsOn = (req, res) => {
    console.log(req)
    const sql = "update goods_cms set g_status='已上架' where gid=?"
    db.query(sql, req.body.gid, (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('上架失败')
        res.cc('上架成功', 0)
    })
}
//下架商品
exports.setgoodsOff = (req, res) => {
    console.log(req)
    const sql = "update goods_cms set g_status='已下架' where gid=?"
    db.query(sql, req.body.gid, (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('下架成功')
        res.cc('下架成功', 0)
    })
}

//删除文章分类处理函数接口
exports.deleteCates = (req, res) => {
    console.log(req)
    const sql = 'update ev_article_cate set is_delete=1 where aid=?'
    db.query(sql, req.body.aid, (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('删除失败')
        res.cc('删除成功', 0)
    })
}

//根据id查询分类
exports.getArtCateById = (req, res) => {
    const sql = 'select * from ev_article_cate where id=? and is_delete=0'
    db.query(sql, req.params.id, (err, result) => {
        if (err) return res.cc(err)
        if (result.length !== 1) return res.cc('查询失败')
        res.send({
            status: 0,
            message: 'ok',
            data: result[0]
        })
    })
}

//编辑商品信息
exports.modifyGoods = (req, res) => {
    console.log(req.body)
    const sql = 'update goods_cms set? where gid=?'
    db.query(sql, [req.body, req.body.gid], (err, result) => {
        if (err) return res.cc(err)
        if (result.affectedRows !== 1) return res.cc('修改商品失败')
        res.cc('修改成功', 0)
    })

}