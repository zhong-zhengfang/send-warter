//导入express
const express = require('express')

//实例化express
const app = express()
//导入cors中间件
const cors = require('cors')

//配置cors中间件
app.use(cors())
//在路由之前封装cc函数
app.use((req, res, next) => {
    res.cc = (err, status=1) => {
        res.send({
            status,
            message:err instanceof Error?err.message:err
        })
    }
    next()
})
app.use(express.static('./public/images'));

//导入express-jwt
const expressJWT = require('express-jwt')
//导入配置文件
const config = require('./config')
//配置解析token中间件
app.use(expressJWT({secret:config.jwtSecretKey}).unless({path:[/^\/api\//,/^\/public\//]}))

//导入验证表单模块
const joi = require('joi')

//配置解析表单数据中间件
app.use(express.urlencoded({ extended: false }))

//导入用户登录路由模块
const cmsLogininRouter = require('./router/cms_login')
app.use('/api', cmsLogininRouter)

//导入用户信息路由模块
const userinfoRouter = require('./router/cms_userInfo')
app.use('/my', userinfoRouter)

//导入商品管理路由模块
const goodsRouter = require('./router/goods')
app.use('/goods', goodsRouter)

//导入商品管理路由模块
const orderRouter = require('./router/order')
app.use('/order', orderRouter)

//导入会员管理路由模块
const vipRouter = require('./router/cms_vip')
app.use('/vip', vipRouter)

//导入会员管理路由模块
const sideRouter = require('./router/side')
app.use('/side', sideRouter)

//导入押金管理路由模块
const depositRouter = require('./router/deposit')
app.use('/deposit', depositRouter)
// *************************************************************************
//以下部分为送水站端小程序接口

//导入送水站登录路由模块
const sideLogininRouter = require('./router/side_login')
app.use('/api', sideLogininRouter)

//导入送水站获取订单数据路由模块
const appsideRouter = require('./router/appside')
app.use('/appside', appsideRouter)

// *************************************************************************
//以下部分为客户端小程序接口
//获取商品列表信息
const userSideRouter = require('./router/userSide')
app.use('/api', userSideRouter)

//导用户端登录路由模块
const vipLogininRouter = require('./router/vip_login')
app.use('/api', vipLogininRouter)

//导入送水站获取订单数据路由模块
const vipInfoRouter = require('./router/vip')
app.use('/vip', vipInfoRouter)

//全局错误级别中间件
app.use((err, req, res, next) => {
    if(err instanceof joi.ValidationError) return res.cc(err)
    if(err.name === 'UnauthorizedError') return res.cc('身份认证失败')
    res.cc(err)
})
//启动服务器
// app.listen(3000,() => {
//     console.log('服务器启动')
// })
// 192.168.1.101
app.listen(3000, '192.168.43.66');