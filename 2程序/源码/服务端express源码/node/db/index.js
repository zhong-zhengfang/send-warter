//导入mysql模块
const mysql = require('mysql')

//新建数据库连接池
const db = mysql.createPool({
    host:'127.0.0.1',
    user:'root',
    password:'root',
    database:'water'
})

module.exports = db