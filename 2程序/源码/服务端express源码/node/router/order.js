const express = require('express')
//创建路由
const router = express.Router()

//获取
//导入路由处理函数
const order_handler = require('../router_handler/order')
router.get('/getOrderList',order_handler.getOrderList)

//暴露路由对象 
module.exports = router