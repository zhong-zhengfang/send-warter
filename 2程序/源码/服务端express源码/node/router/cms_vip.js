const express = require('express')
//创建路由
const router = express.Router()

//获取
//导入路由处理函数
const vip_handler = require('../router_handler/cms_vip')
router.get('/getVipList',vip_handler.getVipList)

//暴露路由对象 
module.exports = router