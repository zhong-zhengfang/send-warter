const express = require('express')
//创建路由
const router = express.Router()

//导入验证数据中间件
const expressJoi = require('@escook/express-joi')

//导入表单验证规则对象
const { vip_login_schema } = require('../schema/cms_login')

//导入路由处理函数
const vip_login_handler = require('../router_handler/vip_login')
//登录
router.post('/viplogin', expressJoi(vip_login_schema), vip_login_handler.viplogin)
//获取商品列表
router.get('/vipgetGoods',vip_login_handler.vipgetGoods)
//获取商品详情
router.get('/vipgetGoodsDetail',vip_login_handler.vipgetGoodsDetail)

//暴露路由对象 
module.exports = router