const express = require('express')
//创建路由
const router = express.Router()

//获取
//导入路由处理函数
const deposit_handler = require('../router_handler/deposit')
router.get('/getDepositList',deposit_handler.getDepositList)
//退押金
router.put('/backDeposit',deposit_handler.backDeposit)
//暴露路由对象 
module.exports = router