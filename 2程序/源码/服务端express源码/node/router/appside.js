const express = require('express')
//创建路由
const router = express.Router()

//获取
//导入路由处理函数
const appside_handler = require('../router_handler/appside')
router.post('/getappSideList',appside_handler.getappSideList)//获取列表
router.put('/setOrderStart',appside_handler.setOrderStart)//接单，更变状态
router.put('/setOrderArrive',appside_handler.setOrderArrive)//接单，更变状态

//暴露路由对象 
module.exports = router