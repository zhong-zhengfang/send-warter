const express = require('express')
//创建路由
const router = express.Router()
//导入规则验证模块
const expressJoi = require('@escook/express-joi')
//获取全部水站列表
const side_handler = require('../router_handler/side')
const { update_password_schema} = require('../schema/user')
router.get('/getSideList',side_handler.getSideList)
router.get('/getSideInfo',side_handler.getSideInfo)
//修改水站信息
router.put('/modifySideInfo',side_handler.modifySideInfo)
//修改密码
router.put('/updatepwd',expressJoi(update_password_schema), side_handler.updatePassword)

//暴露路由对象 
module.exports = router