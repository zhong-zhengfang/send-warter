const express = require('express')
//创建路由
const router = express.Router()

//导入验证数据中间件
const expressJoi = require('@escook/express-joi')

//导入表单验证规则对象
const { side_login_schema } = require('../schema/cms_login')

//导入路由处理函数
const side_login_handler = require('../router_handler/side_login')
//登录
router.post('/sidelogin', expressJoi(side_login_schema), side_login_handler.sidelogin)

//暴露路由对象 
module.exports = router