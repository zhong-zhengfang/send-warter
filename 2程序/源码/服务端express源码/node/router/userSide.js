const express = require('express')
//创建路由
const router = express.Router()

//导入路由处理函数
const userSide_handler = require('../router_handler/userSide')
router.get('/getGoodsList',userSide_handler.getGoodsList)

//暴露路由对象 
module.exports = router