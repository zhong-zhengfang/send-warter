const express = require('express')
//创建路由
const router = express.Router()

//获取用户信息
const vip_handler = require('../router_handler/vip')

router.get('/getVipInfo',vip_handler.getVipInfo)
//发起订单
router.post('/addOrder',vip_handler.addOrder)

//获取订单
router.post('/getmyOrder',vip_handler.getmyOrder)
//暴露路由对象 
module.exports = router