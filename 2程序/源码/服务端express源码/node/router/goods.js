const express = require('express')
//创建路由
const router = express.Router()
var multer = require('multer');

// Multer diskStorage setting
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/images');
    },
    filFilter: function (req, file, cb) {
        var typeArray = file.mimetype.split('/');
        var fileType = typeArray[1];
        if (fileType == 'jpg' || fileType == 'png' || fileType == 'png') {
            cb(null, true);
        } else {
            cb(null, false)
        }
    },
    filename: function (req, file, callback) {
        //fieldname 为文件域的名称 
        callback(null, file.fieldname + '_' + Date.now() + '.jpg');
    }
});
var upload = multer({ storage: storage });

//导入路由处理函数
const goods_handler = require('../router_handler/goods')
//上架商品
router.post('/addGoods',goods_handler.addGoods)
//修改商品
router.put('/modifyGoods',goods_handler.modifyGoods)
//删除商品
router.put('/deleteGoods',goods_handler.deleteGoods)
//上架商品
router.put('/setgoodsOn',goods_handler.setgoodsOn)
//下架商品
router.put('/setgoodsOff',goods_handler.setgoodsOff)
//获取商品列表
router.get('/getGoods',goods_handler.getGoods)
//photo为上传文件域的名称。
router.post('/uploadPic', upload.single('file'), goods_handler.uploadPic)
//暴露路由对象 
module.exports = router