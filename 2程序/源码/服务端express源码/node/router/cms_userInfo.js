const express = require('express')
const router = express.Router()

//导入规则验证模块
const expressJoi = require('@escook/express-joi')
const {update_userinfo_schema, update_password_schema} = require('../schema/user')
//导入路由处理函数模块
const userinfo_handler = require('../router_handler/cms_userInfo')
router.get('/userinfo', userinfo_handler.getUserInf)

router.post('/userinfo', expressJoi(update_userinfo_schema),userinfo_handler.updateUserInfo)

router.post('/updatepwd',expressJoi(update_password_schema), userinfo_handler.updatePassword)

module.exports = router