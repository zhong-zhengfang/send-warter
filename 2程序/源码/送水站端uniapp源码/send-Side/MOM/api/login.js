
import {myRequest} from '@/utils/http.js'
 
export function login (config) {  //登录
	return myRequest({
		url:'/admin/check',
		method:'post',
		data:{
			KEYDATA : "qq313596790fh" + config.username + ",fh," + config.password,
			tm : new Date().getTime(),
		},
		header:{
			'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
	})
}

export function loginOut(config){
	return myRequest({
		url:'/main/logout',
		method:'post',
		data:config
	})
}