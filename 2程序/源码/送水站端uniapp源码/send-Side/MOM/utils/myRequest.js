export const myRequest = (option) => {//传对象
	return new Promise((resolve, reject) => {
		uni.request({
			url:option.url,
			method:option.method || 'GET',//不传默认为GET
			data:option.data || {},//不传默认为空
			header:option.header || { 'Content-Type' : 'application/json'},
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}