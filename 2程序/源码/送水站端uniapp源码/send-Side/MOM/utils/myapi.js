const BaseUrl = 'http://192.168.43.66:3000'

//首页-轮播图、公告、资讯
export const getIndex = {
	url: `${BaseUrl}/api.php/Index/apiIndex`
}

//筛选（抢工单）筛选类别
export const getScreen = {
	url: `${BaseUrl}/api.php/AjaxReturn/apiGetSetting`
}

//登录
export const login = option => ({
	url: `${BaseUrl}/api/sidelogin`,
	data: option,
	method: 'POST',
	header: {
		'content-type': 'application/x-www-form-urlencoded'
	},
})

//获取订单
export const getOrderList = (token,data) => ({
	url: `${BaseUrl}/appside/getappSideList`,
	method: 'POST',
	data:data,
	header: {
		'Authorization':token,
		'content-type': 'application/x-www-form-urlencoded'
	},
})

//接单
export const setOrderStart = (token,data) => ({
	url: `${BaseUrl}/appside/setOrderStart`,
	method: 'PUT',
	data:data,
	header: {
		'Authorization':token,
		'content-type': 'application/x-www-form-urlencoded'
	},
})

//已送达
export const setOrderArrive = (token,data) => ({
	url: `${BaseUrl}/appside/setOrderArrive`,
	method: 'PUT',
	data:data,
	header: {
		'Authorization':token,
		'content-type': 'application/x-www-form-urlencoded'
	},
})

//获取水站点信息
export const getSideInfo = (token) => ({
	url: `${BaseUrl}/side/getSideInfo`,
	header: {
		'Authorization':token
	},
})

//修改水站信息
export const modifySideInfo = (token,data) => ({
	url: `${BaseUrl}/side/modifySideInfo`,
	method: 'PUT',
	data:data,
	header: {
		'Authorization':token,
		'content-type': 'application/x-www-form-urlencoded'
	},
})

//修改水站信息
export const updatePassword = (token,data) => ({
	url: `${BaseUrl}/side/updatepwd`,
	method: 'PUT',
	data:data,
	header: {
		'Authorization':token,
		'content-type': 'application/x-www-form-urlencoded'
	},
})
