import App from './App'
// main.js，注意要在use方法之后执行
import uView from 'uview-ui'
import store from "@/store";
import mixins from "@/mixins";
import {myRequest} from "@/utils/myRequest.js"
import {
	baseURL
} from '@/utils/base.js'; //导入接口的前缀地址
import {$delayed,$jsontoQuery} from "@/utils/tool.js"
// #ifndef VUE3
import Vue from 'vue'
Vue.prototype.$myRequest = myRequest
Vue.use(uView)
Vue.config.productionTip = false
Vue.prototype.$baseURL = baseURL
// 延迟
Vue.prototype.$delayed=$delayed
Vue.prototype.$jsontoQuery=$jsontoQuery
Vue.mixin(mixins);
// 需要在Vue.use(uView)之后执行
uni.$u.setConfig({
	// 修改$u.config对象的属性
	config: {
		// 修改默认单位为rpx，相当于执行 uni.$u.config.unit = 'rpx'
		// unit: 'rpx'
	},
	// 修改$u.props对象的属性
	props: {
		// 修改radio组件的size参数的默认值，相当于执行 uni.$u.props.radio.size = 30
		radio: {
			size: 15
		},
    icon:{
      size:"80rpx"
    }
		// 其他组件属性配置
		// ......
	}
})

App.mpType = 'app'
const app = new Vue({
    ...App,
    store
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif