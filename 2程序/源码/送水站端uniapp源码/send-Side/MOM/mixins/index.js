export default {
	computed: {
		// 最小时间
		minDateTime() {
			return +new Date(1970, 1, 1);
		},
		// 最大时间
		maxDateTime() {
			var date = new Date();
			const year = date.getFullYear();
			return +new Date(year + 100, 1, 1);
		},

	},
	methods: {
		changeTime(time, format) {
			var t = new Date(time);
			var tf = function(i) {
				return (i < 10 ? '0' : '') + i
			};
			return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a) {
				switch (a) {
					case 'yyyy':
						return tf(t.getFullYear());
						break;
					case 'MM':
						return tf(t.getMonth() + 1);
						break;
					case 'mm':
						return tf(t.getMinutes());
						break;
					case 'dd':
						return tf(t.getDate());
						break;
					case 'HH':
						return tf(t.getHours());
						break;
					case 'ss':
						return tf(t.getSeconds());
						break;
				}
			})
		},
		// 获取实际高度
		heightConversion(elementHeight) {
			console.log(232131232)
			elementHeight = uni.upx2px(elementHeight);
			const windowHeight = this.$u.sys().windowHeight;
			const height = windowHeight - elementHeight;
			return this.$u.addUnit(height, "px");
		},
		// 拨打电话 
		handleCallPhone(phone) {
			phone += "";
			//#ifdef MP-WEIXIN
			uni.makePhoneCall({
				phoneNumber: phone,
			});
			//#endif
			//#ifdef APP-PLUS
			plus.device.dial(phone, true);
			//#endif
			//#ifdef H5
			var a = document.createElement("a");
			a.href = "tel:" + phone;
			var event;
			if (window.MouseEvent) {
				event = new MouseEvent("click");
			} else {
				event = document.createEvent("MouseEvents");
				event.initMouseEvent(
					"click",
					true,
					false,
					window,
					0,
					0,
					0,
					0,
					0,
					false,
					false,
					false,
					false,
					0,
					null
				);
			}
			a.dispatchEvent(event);
			//#endif
		},


		/**
		 * 描述 图片预览
		 * @param {any} index
		 * @param {any} imgs
		 * @returns {any}
		 */
		handlePreviewImage(index, imgs) {
			uni.previewImage({
				current: index,
				urls: imgs,
			});
		},

		// 导航
		goMAP(item) {
			uni.openLocation({
					latitude: Number(item.olat), //要去的纬度-地址       
					longitude: Number(item.olot), //要去的经度-地址
					name: "北京", //地址名称
					address: "长安街", //详细地址名称
					success: function() {
						console.log('导航成功');
					},
					fail: function(error) {
						console.log(error)
					}

				})
			},
		},
		}
