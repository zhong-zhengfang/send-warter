const BaseUrl = 'http://192.168.43.66:3000'

//首页-轮播图、公告、资讯
export const getIndex = {
	url: `${BaseUrl}/api.php/Index/apiIndex`
}

//筛选（抢工单）筛选类别
export const getScreen = {
	url: `${BaseUrl}/api.php/AjaxReturn/apiGetSetting`
}

//登录
export const login = option => ({
	url: `${BaseUrl}/api/viplogin`,
	data: option,
	method: 'POST',
	header: {
		'content-type': 'application/x-www-form-urlencoded'
	},
})

//获取订单
export const vipgetGoods = {
	url: `${BaseUrl}/api/vipgetGoods`
}

//提交订单
export const addOrder = (token,data) => ({
	url: `${BaseUrl}/vip/addOrder`,
	data: data,
	method: 'POST',
	header: {
		'Authorization':token,
		'content-type': 'application/x-www-form-urlencoded'
	},
})

//获取订单
export const getmyOrder = (token,data) => ({
	url: `${BaseUrl}/vip/getmyOrder`,
	data: data,
	method: 'POST',
	header: {
		'Authorization':token,
		'content-type': 'application/x-www-form-urlencoded'
	},
})

//获取订单详情
export const vipgetGoodsDetail = (data) => ({
	url: `${BaseUrl}/api/vipgetGoodsDetail`,
	data:data,
})


//获取水站点信息
export const getVipInfo = (token) => ({
	url: `${BaseUrl}/vip/getVipInfo`,
	header: {
		'Authorization':token
	},
})

