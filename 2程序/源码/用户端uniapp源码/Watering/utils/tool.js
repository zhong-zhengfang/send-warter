var timer
export const $delayed=(callvack = () => {}) =>{
	if (timer) return;
	timer = setTimeout(() => {
		callvack();
		timer = null;
	}, 400)
}
export const $jsontoQuery=(json)=>{
  let str = "";
  let query = "";
  for(let i in json){//i是对象的键值
    for(let j in json[i]){//j是属性名
      str+= j + "=" + json[i][j] + "&"//json[i][j]是属性值
    }
  }
  query=str.substring(0,str.length-1);
  return query?'?'+query:''
}
